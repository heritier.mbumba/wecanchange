from django.contrib import admin
from .models import Organisation, Page, OrganisationToCategory
from category.models import MainCategory


class OrganisationToCategoryInline(admin.TabularInline):
    model = OrganisationToCategory,
    extra = 1


class OrganisationAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'email', 'phone')
    # inlines = [OrganisationToCategoryInline]

    class Meta:
        model = Organisation

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        obj.save()


class PageAdmin(admin.ModelAdmin):
    list_display = ('name', "belong_to")
    prepopulated_fields = {"slug": ("name",)}
    search_fields=['organisation__name']


admin.site.register(Organisation, OrganisationAdmin)
admin.site.register(Page, PageAdmin)

