# Generated by Django 2.2.4 on 2019-11-20 18:21

from django.db import migrations, models
import django.db.models.deletion
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0003_taggeditem_add_unique_index'),
        ('mainsite', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='opportunity',
            options={'verbose_name': 'Opportunies', 'verbose_name_plural': 'Opportunies'},
        ),
        migrations.AddField(
            model_name='banner',
            name='banner',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AddField(
            model_name='banner',
            name='file',
            field=models.FileField(default='banners/default.png', upload_to='banners/'),
        ),
        migrations.AddField(
            model_name='banner',
            name='position',
            field=models.CharField(choices=[('topbar', 'Top Bar'), ('sidebar', 'Side Bar'), ('underheadlinenews', 'Under Headline News'), ('undercompanynews', 'Under Company News'), ('undernews', 'Under News')], default='sidebar', max_length=20),
        ),
        migrations.AlterField(
            model_name='banner',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='companies.Company'),
        ),
        migrations.AlterField(
            model_name='banner',
            name='ngo_npo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ngo_npo_profile.Organisation'),
        ),
        migrations.AlterField(
            model_name='banner',
            name='service_provider',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='service_provider.ServiceProvider'),
        ),
        migrations.AlterField(
            model_name='event',
            name='event_main_guest',
            field=models.FileField(default='noprofile.png', upload_to='events/'),
        ),
        migrations.AlterField(
            model_name='opportunity',
            name='company_logo',
            field=models.FileField(default='wecanchangeopportunity.jpg', upload_to='opportunities/'),
        ),
        migrations.AlterField(
            model_name='opportunity',
            name='expect',
            field=models.TextField(blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='post',
            name='expect',
            field=models.TextField(help_text='excerpt content limit 255 characteres', max_length=255),
        ),
        migrations.AlterField(
            model_name='post',
            name='image_url',
            field=models.FileField(default='postnoimage.png', upload_to='post/'),
        ),
        migrations.RemoveField(
            model_name='post',
            name='tags',
        ),
        migrations.AddField(
            model_name='post',
            name='tags',
            field=taggit.managers.TaggableManager(help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
        migrations.DeleteModel(
            name='Newsletter',
        ),
    ]
