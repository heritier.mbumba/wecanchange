from .models import Category, Event, Banner


def categoriesList(request):

    categories_list = Category.objects.all().order_by('name')

    return {
        'categoriesList': categories_list,
    }


def events(request):

    events_list = Event.objects.all().order_by('-start_date')

    return {
        'events': events_list
    }


def banners(request):
    banners = Banner.objects.all()
    return {
        'banners':banners
    }