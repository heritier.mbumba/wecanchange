from django.contrib import admin
from .models import MainCategory


class MainCategoryAdmin(admin.ModelAdmin):
    list_display = ('categoryName', )
    prepopulated_fields = {"categorySlug": ("categoryName",)}


admin.site.register(MainCategory)


