from django.db import models

# Create your models here.


class MainCategory(models.Model):
    categorName = models.CharField(max_length=100)
    categorySlug = models.SlugField(max_length=100, unique=True)

    class Meta:
        verbose_name = 'Profiles Categories'
        verbose_name_plural = 'Profiles Categories'

    def __str__(self):
        return self.categorName

